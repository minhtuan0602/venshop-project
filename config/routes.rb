Rails.application.routes.draw do
  resources :users
  resources :products
  resources :sessions, only: [:new, :create, :destroy]
  resources :categories
  # resources :products, only: [:create, :destroy. :show]
  resources :images do
      collection { post :upload_image } end


  #get 'users/new'
  # root  'static_pages#home'

  # match 'cart/add', to: "carts#add", as: => "cart_add"
  match 'sendmail',     :controller => :carts,  :to => :sendmail, :via => :get, :as => :send_mail
  match 'checkout',     :controller => :carts,  :to => :checkout, :via => :get, :as => :checkout
  match 'add/:id',      :controller   =>:carts, :to => :add,     :via  => :get,     :as =>:cart_add
  match 'cart/index',   :controller   =>:carts, :to => :index,   :via  => :get,     :as =>:cart_index
  match 'cart/destroy/:id', :controller   =>:carts, :to => :destroy, :via => :get,  :as => :cart_destroy
  match 'remove/:id',   :controller   =>:carts, :to => :remove,   :via => :delete, :as => :cart_remove
  match 'update/:id',   :controller => :carts, :to => :update, :via => :put, :as => :cart_update
  match 'detail/:id',   :controller => :products, :to => :detail, :via => :get, :as => :product_detail

  root  'products#show'
  match 'search', :controller => :products, :to => :index, :via => :post, :as => :search_product

  match '/signup',  to: 'users#new',            via: 'get'
  match '/signin',  to: 'sessions#new',         via: 'get'
  match '/signout', to: 'sessions#destroy',     via: 'delete'

  match '/help',    to: 'static_pages#help',    via: 'get'
  match '/about',   to: 'static_pages#about',   via: 'get', as: 'about'
  match '/contact', to: 'static_pages#contact', via: 'get'
  match '/uploadfile', to: 'upload#uploadfile', via: 'post'

  get 'static_pages/home'
  get 'static_pages/help'
  #get "static_pages/about"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
