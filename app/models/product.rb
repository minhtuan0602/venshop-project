class Product < ActiveRecord::Base
	searchable do
    text :title
    text :author
    text :description
    text :isbn
  end
  belongs_to :user
  belongs_to :category
  default_scope -> { order('created_at DESC') }
  validates :image, presence: true
  validates :title, presence: true, length: { maximum: 5000 }
  validates :user_id, presence: true
  validates :category_id, presence: true
end
