class StaticPagesController < ApplicationController
  def home
    @product = current_user.products.build if signed_in?
  end

  def help
  end

  def about
  end

end
