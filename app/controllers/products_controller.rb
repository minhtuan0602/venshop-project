class ProductsController < ApplicationController
  def show
    @categories = Category.all
    @products = Product.paginate(page: params[:page], per_page: 10)
  end

  def index
    @categories = Category.all
    @search = Product.search do
      keywords params[:search]
    end
    @products = @search.results
  end

  def create
    @product = current_user.products.build(product_params)
    # image = uploadfile(@product.image)
    # image_convert = "/#{image.split('/images').join}"
    # puts image_convert
    # puts "==========================================="
    # @product.image = image

    # <%= f.label :image %>
    # <%= f.file_field :image %>

    @product.user_id = current_user.id
    if @product.save
      flash[:success] = "Product created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end

  def detail
    @product = Product.find(params[:id])
    @products = Product.where(category: @product.category_id).take(5)
    @categories = Category.all
  end

  def destroy
  end



  private

    def product_params
      params.require(:product).permit(:title, :price, :description, :amount, :author, :isbn, :category_id, :image)
    end
end

