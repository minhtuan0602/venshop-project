class CartsController < ApplicationController

  def index
    @products = Product.all
  end

  def add
    session[:cart] ||= []
    check = false
    session[:cart].each do |a|
      if a["product_id"] == params[:id] then
        a["amount"] = a["amount"] + 1
        check = true
      end
    end

    session[:cart]. push ({"product_id" => params[:id], "amount" => 1}) unless check

    product_title = Product.find(params[:id]).title
    flash[:success] = "add success product \"#{product_title}\" to your cart"
    redirect_to root_path
  end

  def remove
    product_title = Product.find(params[:id]).title
    session[:cart].each do |a|
      if a["product_id"] == params[:id] then
        a["amount"] = 0
      end
    end
    flash[:success] = "delete success product \"#{product_title}\" from your cart"
    redirect_to cart_index_path
  end

  def update
    product_title = Product.find(params[:id]).title
    if params[:amount][:quantity].blank? then
      flash[:error] = "edit fail quantity of product \"#{product_title}\" "
    else
      quantity = params[:amount][:quantity].to_i
      session[:cart].each do |a|
        if a["product_id"] ==  params[:id] then
          a["amount"] = quantity
        end
      end
      flash[:success] = "edit success quantity of product \"#{product_title}\" "
    end

    redirect_to cart_index_path
  end

  def sendmail
    @products = Product.all
  end

  def checkout
    @products = []
    @email = params[:email]
    @total_money = 0;
    @allproduct = Product.all
    session[:cart].each do |a|
      @products.push ({"product_id" => a["product_id"], "amount" => a["amount"]})
      @total_money += Product.all.find(a["product_id"]).price * a["amount"]
    end
    session.clear
    Sendmail.sendmail(@products, @email, @total_money).deliver
  end

  def destroy
    session.clear
    if params[:id] == "0" then
      flash[:success] = "Cancel your cart success"
    else
      flash[:success] = "Thank you for shopping"
    end

    redirect_to root_path
  end
end
