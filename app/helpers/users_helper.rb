module UsersHelper
  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user, options = { size: 52 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar img-responsive")
  end

  def gravatar_for_product(link, options = {size: 50})
    size = options[:size]
    link_url = "#{link}?s=#{size}"
    image_tag(link_url, alt: "waiting", class: "gravatar_product img-responsive", size: "#{size}x#{size}")
  end
end
