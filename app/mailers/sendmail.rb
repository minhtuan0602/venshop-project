class Sendmail < ActionMailer::Base
  default from: "from@example.com"

  def sendmail(products, email, total_money)
    @allproduct = Product.all
    @products = products
    @email = email
    @total_money = total_money
    mail(from: "admin@gmail.com", to: @email, subject: "Venshop Cart'infomation")
  end
end
