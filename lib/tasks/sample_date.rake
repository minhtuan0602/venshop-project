require 'rexml/document'
include REXML

namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    make_users
    make_categories
    make_product
  end
end

def make_users
  admin = User.create!(name:     "Admin",
                       email:    "admin@gmail.com",
                       password: "123456",
                       password_confirmation: "123456",)
  User.create(name: "Example User",
                       email: "example@railstutorial.org",
                       password: "foobar",
                       password_confirmation: "foobar")
end


def make_categories
  Category.create!(title: "Children")
  Category.create!(title: "Comic")
  Category.create!(title: "Computer")
  Category.create!(title: "Education")
  Category.create!(title: "Health")
  Category.create!(title: "History")
  Category.create!(title: "Romance")
  Category.create!(title: "Sport")
end

def make_product
  Category.all.each {
    |a|
    cat_id = a.id
    xmlfile = File.new("xml/" + a.title + ".xml")
    xmldoc = Document.new(xmlfile)
    user = User.first
    for a in 1..10 do
      title = ""
      price = ""
      description = ""
      image = ""
      amount = ""
      author = ""
      isbn = ""
      i = 0
      xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/ItemAttributes/Title"){ |e| title = e.text }
      xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/ItemAttributes/ListPrice/FormattedPrice"){ |e| price = e.text }
      xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/EditorialReviews/EditorialReview/Content"){ |e| description = e.text }
      xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/LargeImage/URL"){ |e| image = e.text }
      xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/ItemAttributes/ListPrice/Amount"){ |e| amount = e.text }
      xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/ItemAttributes/Author"){
        |e|
        if i.zero? then
          author = e.text
        else
          author = author + " & " + e.text
        end
        i += 1
      }
      xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/ItemAttributes/ISBN"){ |e| isbn = e.text }
      if image.empty? then
        image = "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcScz1MAxzmZW_mhhGYv3NSzNzg_M_b2Y5viYJ21-sVsYvWRJzbx"
      end
      Product.create!(title: title, price: price.delete('$').to_f, description: description,  amount: amount.to_i, author: author, isbn: isbn, user_id: user.id, category_id: cat_id, image: image)
    end
  }


end