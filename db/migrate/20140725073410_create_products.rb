class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.integer :price
      t.string :description
      t.integer :amount
      t.string :author
      t.string :isbn
      t.integer :user_id
      t.integer :category_id

      t.timestamps
    end
    add_index :products, [:user_id, :created_at]
    add_index :products, [:category_id, :created_at]
  end
end
