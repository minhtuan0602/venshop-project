class ChangeTypeFormatPriceInProduct < ActiveRecord::Migration
  def change
  	change_column :products, :price, :Float
  end
end
