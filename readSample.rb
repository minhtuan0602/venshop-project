#!/usr/bin/ruby -w

require 'rexml/document'
include REXML

xmlfile = File.new("xml/Computer.xml")
xmldoc = Document.new(xmlfile)

# Now get the root element
root = xmldoc.root
for a in 0..10 do
	i = 0
	title = ""
  xmldoc.elements.each("ItemSearchResponse/Items/Item[#{a}]/ItemAttributes/Author"){
    |e| 
    if i.zero? then
    	title = e.text
    else
			title = title + " & " + e.text
		end
		i += 1
  }
  puts title
end